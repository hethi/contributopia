+++

date = "2016-12-05T14:41:00+01:00"
title = "Services"
author = "Framasoft"
draft = false
type = "page"
id = "-services"
[menu]
     [menu.main]
        name = "Services"
        weight = 1
        identifier = "services"
+++

</p>
{{% grid class="row" %}}
{{% grid class="col-sm-8 col-sm-offset-4" %}}

# Services
## Create and offer tools

Welcome to a world where tools are, from their very conception, destined to foster peaceful exchanges!

In 2017 and 2018, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> intends to be part of the development and hosting of new web services, designed to respect users, their freedom, their exchanges and their networks.

These services will be developed in collaboration with users in order to enrich their experience.

{{% /grid %}}
{{% /grid %}}
<!-- service framasite -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/services-framasite.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Framasite, enter webspace

Creating a webpage, a website, a blog or even a wiki&hellip; all too often these modes of expression and means of sharing remain complex to tame, even more so when we choose not to share our private life with lucrative platforms.

<a href="https://frama.site"><b class="violet">Frama</b><b class="vert">site</b></a> offers online equipment that will enable you to build your place on the web. This service gives you access to a place for expression where the content is your own in a hosting space which respects the data (and the privacy) of the people that visit, participate in and contribute to your site.

<a href="https://frama.site"><b class="violet">Frama</b><b class="vert">site</b></a> will not not be perfect when it is launched. But thanks to your feedback, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> will be able to simplify the usage and contribute to the development of this software enabling the creation of ethical websites&hellip; for each and every one of us.

{{% /grid %}}
{{% /grid %}}
<!-- service framameet -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/services-framameet2.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Framameet, encouraging gatherings and meetings

Online exchanges can sometimes lead to face to face meetings, far from our keyboards. When we organise these meetings with tools like _Facebook_ or through platforms like _MeetUp_, we abandon the personal information of the members of the group to these data devouring ogres.

The free libre software community is interested in creating tools of this type, all the while respecting the fundamental freedoms of the people wanting to gather.

<a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> intends to accompany one such project and contribute to its development&hellip; right up to the deployment of a libre service, open to all, and in the service of our society.

{{% /grid %}}
{{% /grid %}}
<!-- service framapetitions -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/services-framapetitions2.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}
    
### Framapetitions, making our opinions heard

Online petitions constitute a tool that is at once strategic and delicate. Our collections of opinions on the world should not be locked up by some obscure software nor exploited by profit driven businesses.

When <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> created our service for online forms <a href="https://framaforms.org"><b class="violet">Frama</b><b class="vert">forms</b></a>, it was clear that an adaptation of the code for the service could lead to Framapetitions. It was nevertheless necessary to wait and see how well this service stood up to a massive userbase.

Thanks to more than a years experience of hosting <a href="https://framaforms.org"><b class="violet">Frama</b><b class="vert">forms</b></a>, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> will create and then host Framapetitions, which will require your contributions in the form of tests, feedback and support.


{{% /grid %}}
{{% /grid %}}
<!-- service framapetube -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/services-framatube.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Framatube, let's break the hegemony of YouTube

The hosting of videos, when it is centralised on a platform, poses important technical and economic challenges. Too many videos will cost you dear in webspace. If your site is a success there will be significant traffic (with a traffic jam on your server!) and you may find yourself having expensive bills to pay.

So rather than using the centralised model that has allowed _YouTube_, _Netflix_ (and others&hellip;) to become indispensable, why not be inspired by the methods that led to the creation of the internet: decentralisation and peer-to-peer sharing!

The free libre software _PeerTube_ will allow a federation of video hosts to transmit videos peer-to-peer. So each computer that receives a video will send it on to others at the same time, and each host will decide his or her own rules of play (conditions of use, moderation, monetisation&hellip;) all the while linked to other hosts of the federation.

<a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> is wagering on _PeerTube_ by financing its development, not so that the everyone's videos will be on a Framatube, but so that artists, associations, organisations, institutions and the media can host their own independent video platforms and contribute in turn to this project.

{{% /grid %}}
{{% /grid %}}
<p>
