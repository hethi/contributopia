+++

date = "2016-12-05T14:41:00+01:00"
title = "Essaimage"
author = "Framasoft"
draft = false
type = "page"
id = "-essaimage"
[menu]
     [menu.main]
        name = "Essaimage"
        weight = 1
        identifier = "essaimage"
+++

</p>
{{% grid class="row" %}}
{{% grid class="col-sm-8 col-sm-offset-4" %}}

# Essaimage
## Transmettre les savoir-faire

Bienvenue dans un monde où le cloud éthique devient à la portée de tou·te·s&nbsp;!

Pour 2018 et 2019, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> désire favoriser des actions qui encouragent l’autonomie numérique, pour mettre à la portée du plus grand nombre un hébergement de confiance solidaire de nos vies numériques.

Un processus d’essaimage déjà entamé et que l’on doit affiner pour proposer une alternative concrète et locale aux entreprises-silos moissonnant nos données.

{{% /grid %}}
{{% /grid %}}
<!-- chatons -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/essaimage-chatons.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Des <abbr title="Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires">CHATONS</abbr> pour un hébergement local, éthique et solidaire

Né en octobre 2016 à l’initiative de <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a>, le Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires est à la fois un label auto-géré et un outil d’entraide.

Pour le public, ce label <abbr title="Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires">CHATONS</abbr> sert à pouvoir identifier près de chez soi un hébergeur à qui confier ses données, à qui faire confiance. Cette confiance repose sur des engagements stricts, autour de valeurs proclamées telles que l’ouverture, la transparence, la neutralité et le respect des données personnelles.

Pour les membres du collectif, <abbr title="Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires">CHATONS</abbr> est un outil commun où l’on s’entraide, entre autres, par le partage des savoirs juridiques, techniques et relationnels qui permettent de rester dignes de cette confiance accordée par les bénéficiaires des services.

<a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> veut pouvoir prendre le temps de se mettre à disposition du collectif. C’est ce dernier qui décidera des actions à mener ensemble pour se développer. Ceci dans l’espoir d’arriver à ce que, un jour, chacun·e puisse trouver un CHATON près de chez soi&nbsp;!


{{% /grid %}}
{{% /grid %}}
<!-- yunohost -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/essaimage-yunohost.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### YUNOHOST, l’auto hébergement facile

YUNOHOST est un projet libre ayant pour but de permettre à quiconque d’auto-héberger ses services avec un minimum de connaissances techniques. Emails, partage de fichiers, agendas, outils de création et d’organisation collaborative&nbsp;: tout cela s’installe en quelques clics afin d’avoir chez soi le serveur (et les données) de ses proches, son association, son entreprise, son collectif&hellip;

Depuis janvier 2017, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> consacre du temps salarié au développement de ce projet afin de s’assurer qu’un maximum de services libres présentés dans la campagne «&nbsp;<a href="https://degooglisons-internet.org"><b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b></a>&nbsp;» puisse être disponible dans la solution YUNOHOST.

C’est dans l’esprit de cette collaboration avec YUNOHOST que <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> souhaite accompagner et promouvoir cette solution afin qu’elle remporte une adhésion massive&hellip; 


{{% /grid %}}
{{% /grid %}}
<!-- i18n -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/essaimage-i18n.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}
    
### Internationalisation&nbsp;: partager l’expérience hors des frontières

Avec la campagne «&nbsp;<a href="https://degooglisons-internet.org"><b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b></a>&nbsp;», <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> propose plus de trente services web éthiques et alternatifs à un public francophone. Il est hors de propos de traduire les services hébergés par <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a>&nbsp;: cela ferait beaucoup trop de monde et donc de poids sur les épaules d’une petite association française&nbsp;!

Néanmoins, parce qu’il propose une solution (presque) complète, une expérience (relativement) aboutie et qu’il a remporté une adhésion (modestement) remarquable, le projet <a href="https://degooglisons-internet.org"><b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b></a> semble quasiment unique au monde&hellip; et cela pourrait changer&nbsp;!

Il y a là un magnifique chantier à défricher ensemble, celui de partager ces années de _dégooglisation_ pour que d’autres puissent s’en inspirer et l’appliquer dans leur langue, en l’adaptant à leur propre culture.

Bref, il s’agit juste de transformer, ensemble, l’histoire de «&nbsp;<a href="https://degooglisons-internet.org"><b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b></a>&nbsp;» en un commun international. Rien que ça&nbsp;!

{{% /grid %}}
{{% /grid %}}
<!-- winter of code -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/essaimage-woc.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Framasoft _Winter of code_&nbsp;: _winter is coding_

Avec le _Google Summer of code_, le géant du web est bien malin&nbsp;: financer les projets _open source_ de développeurs et développeuses lui permet à la fois de choisir les codes qui vont dans son intérêt, mais aussi de séduire des talents, de les formater à sa culture d’entreprise tout en redorant son blason&nbsp;!

En outre, le monde des libertés numériques devient de plus en plus dépendant des contributions financées par _Google_ et autres géants du Web pour perdurer, et c’est inquiétant.

En proposant le _Winter of Code_, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> désire faire sa part dans l’inversion de cette tendance. L’idée maîtresse est de mettre en relation les communautés de logiciels libres ayant besoin de talents et les stagiaires des métiers numériques cherchant une formation qui ait du sens, et d’apporter un soutien administratif, voire financier.

Tout ceci reste à imaginer avec les parties concernées, mais une chose reste sûre&nbsp;: _Winter is coding_&nbsp;!

{{% /grid %}}
{{% /grid %}}
<p>
