+++

date = "2016-12-05T14:41:00+01:00"
title = "Éducation populaire"
author = "Framasoft"
draft = false
type = "page"
id = "-educ-pop"
[menu]
     [menu.main]
        name = "Éduc pop"
        weight = 1
        identifier = "educ-pop"
+++

</p>
{{% grid class="row" %}}
{{% grid class="col-sm-8 col-sm-offset-4" %}}

# Éducation populaire
## Inspirer les possibles

Bienvenue dans un monde où chacun·e peut partager et accéder aux connaissances&nbsp;!

Vers 2019 et 2020 et après un important travail de rencontres, d’échanges et de collaborations, <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> se propose de se concentrer sur des outils numériques qui faciliteront l’accès aux savoirs communs.

C’est un monde encore bien inconnu, parce qu’il reste à imaginer ensemble, avec les acteurs et actrices qui, chaque jour, concrétisent une société où l’on contribue plus qu’on ne consomme, la société de contribution.

{{% /grid %}}
{{% /grid %}}
<!-- mediation -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/educ-pop-outils.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### Actions de médiation

Aujourd’hui, trouver le service web libre et éthique qui correspond à ses usages demande de nombreuses connaissances et reste difficile d’accès aux personnes les moins à l’aise avec l’outil numérique.

C’est en travaillant de concert avec ce public, des spécialistes de la médiation numérique et des professionnel·le·s du _design_ et de l’expérience utilisateur que <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> désire s’attaquer à la question, tout en proposant des outils et des formations aux actrices et acteurs des réseaux territoriaux qui font de la médiation numérique.

Le but&nbsp;? Créer les outils et les conditions pour faciliter l’accès à un web éthique. 


{{% /grid %}}
{{% /grid %}}
<!-- git -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/educ-pop-git-moldus.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}
    

### Git à la portée de tou·te·s

_Git_ est un outil qui permet de collaborer collectivement sur du code. La manière dont il a été conçu ouvre une magnifique porte d’entrée vers les méthodes, pratiques et états d’esprits de la contribution.

Comment faire pour que les personnes qui ne codent pas puissent profiter de la philosophie d’un tel outil&nbsp;? Comment adapter _git_, son jargon, son interface, etc. afin que tout le monde puisse co-construire et collaborer sur des textes ou d’autres communs contributifs&nbsp;?

Des projets existent et c’est avec eux que <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> désire étudier des solutions.

{{% /grid %}}
{{% /grid %}}
<!-- mooc chatons -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/educ-pop-mooc-chatons.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}


### Un MOOC pour les <abbr title="Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires">CHATONS</abbr>

Devenir un <abbr title="Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires">CHATONS</abbr>, un membre de ce collectif d’organisations qui héberge des services web éthiques, n’est pas une mince affaire&nbsp;: il y a une impressionnante somme de savoirs à acquérir, et de pratiques à connaître.

C’est avec les membres de ce collectif que <a href="https://framasoft.org"><b class="violet">Frama</b><b class="orange">soft</b></a> propose de créer un MOOC, un cours en ligne ouvert et public, pour rassembler et structurer les connaissances à partager&hellip; et que se multiplient les <abbr title="Collectif d’Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires">CHATONS</abbr> dans notre panier commun&nbsp;!

{{% /grid %}}
{{% /grid %}}
<!-- upload -->
{{% grid class="row" %}}
{{% grid class="col-sm-4" %}}

![](/educ-pop-upload.jpg)

{{% /grid %}}
{{% grid class="col-sm-8" %}}

### UPLOAD, Université Populaire du Libre

C’est le projet le plus flou et peut-être le plus fou, de cette Contributopia&nbsp;: ouvrir une Université Populaire du Libre, Ouverte, Accessible et Décentralisée.

L’envie est de créer un outil numérique où chacun·e peut apporter ses savoirs et y puiser des connaissances, voire (rêvons un peu) les faire reconnaître.

Il est difficile de décrire dès aujourd’hui comment cette Université Populaire va se créer, car sa conception dépendra de toutes les bonnes volontés, contributions et partenariats qui s’empareront du projet tout au long du voyage.

{{% /grid %}}
{{% /grid %}}
<p>
